/**
 * MATH
 */

// 1. Pagliacci charges $16.99 for a 13” pizza and $19.99 for a 17” pizza.
// What is the area for each of these pizzas?
// (radius would be the listed size - i.e. 13" - divided by 2)

let areaOfThirteenInch = Math.PI * (13/2) ** 2

console.log(areaOfThirteenInch)

// areaOfThirteenInch = 132.73228961416876

let areaOfSeventeenInch = Math.PI * (17/2) ** 2

console.log(areaOfSeventeenInch)

// areaOfSeventeenInch = 226.98006922186255




// 2. What is the cost per square inch of each pizza?

let pricePerSquareThriteen = 16.99 / areaOfThirteenInch

console.log(pricePerSquareThriteen)

// pricePerSquareThirteen = 0.12800201103580125

let pricePerSquareSeventeen = 19.99 / areaOfSeventeenInch //Fixed the error where Pizza was added incorrectly to this variable in first commit

console.log(pricePerSquareSeventeen)

// pricePerSquareSeventeen = 0.08806940657181973

// I would totally get the 17 inch as it is a better deal



// 3. Using the Math object, put together a code snippet
// that allows you to draw a random card with a value
// between 1 and 13 (assume ace is 1, jack is 11…)

Math.round(Math.random() * 13 + 1)



// 4. Draw 3 cards and use Math to determine the highest
// card

let cardOne = Math.round(Math.random() * 13 + 1)

console.log(cardOne)

// 8

let cardTwo = Math.round(Math.random() * 13 + 1)

console.log(cardTwo)

// 6 

let cardThree = Math.round(Math.random() * 13 + 1)

console.log(cardThree)

// 11

let maxCard = Math.max(cardOne, cardTwo, cardThree)

console.log(maxCard)

// 11


/**
 * ADDRESS LINE
 */

// 1. Create variables for firstName, lastName,
// streetAddress, city, state, and zipCode. Use
// this information to create a formatted address block
// that could be printed onto an envelope.

let firstName = "Dane"
let lastName = "Kutz-Smith"
let streetAddress = "515 S Farr Rd"
let city = "Spokane"
let state = "WA"
let zipCode = "99206"

// 2. You are given a string in this format:
// firstName lastName(assume no spaces in either)
// streetAddress
// city, state zip(could be spaces in city and state)

//forgot to make this a variable in first commit
printAddress = `${firstName} ${lastName}  
${streetAddress}
${city}, ${state} ${zipCode}`

console.log(printAddress)

// 
// Write code that is able to extract the first name from this string into a variable.
// Hint: use indexOf, slice, and / or substring

var str = printAddress

var res = str.slice(0,4)

console.log(res)


/**
 * FIND THE MIDDLE DATE
 */
// On your own find the middle date(and time) between the following two dates:
// 1/1/2020 00:00:00 and 4/1/2020 00:00:00

date1 = new Date("1/1/2020")
date2 = new Date("4/1/2020")
middle = new Date(date2 - (date2-date1)/2)

console.log(middle)

//
// Look online for documentation on Date objects.

// Starting hint:
const endDate = new Date(2019, 3, 1);